# Implementation of Gaming QoE model for Online Mobile Gaming Servicess

This repo provides the implementation of an Opinion model predicting online mobile gaming quality of experience based on network degradations. 

- The model is a parametric based model that can be used for planning or monitoring purpose and based on the network parameters, packetloss, delay and game type provides the quality on MOS scale from 1 to 5 (5 best, 1 worse). 

## How to Use
In order to run the code please use run it based on the parameters of the model as follows:

- Test the model based on the parameters, which requires specifying all network parameters, network parameters packetloss and delay (interaction) as well as game type (Gname) .  To do so, you can run it as the following example:

```
    python OMMOG.py --packetloss=0 --delay=0 --Gname=AOV
```

 For more help run:
 ```
    python test.py -h
```

### Application Range of Parameters 

Please note that the model only works based on the range of parameters used for training the model. 
- Delay: 0 – 1000 (default: 0)
- Packetloss: 0 - 50 (50 stands for 50% packet loss rate), (default: 0)
- Delay Sensitivity Class: none, High, Low, (default: none)
- Game type: none, AOV, Vainglory, PES, CNR, Fortnite, PUBG, CS, (default: None)

Please note that the game type is the abbreviation of the game names as follows, categorized by genre:
- MOBA: Arena of Valor (AOV), Vainglory 
- Sport: Pro Evolution Soccer (PES) 
- Shooting: Cops N Robbers: 3D Pixel FPS (CNR), Fortnite, PlayerUnknown's Battlegrounds (PUBG), CyberSphere



### Output of the model
The model gives you four estimations: 
- Overall Quality
- Interaction (Input) Quality due to delay and packet loss influencing the interaction of players
- Temporal Video Quality due to packet loss and delay (jerkiness)
- The overall quality for the most sensitive game in the training dataset
- The overall quality for the least sensitive game in the training dataset

#### Example 
Here you can find an example for a condition with 1 Mbps, 60 fps, packet loss of 5%, delay of 25 ms, Interaction complexity of "High", and Video Complexity of "Low" level:
```
    python OMMOG.py --packetloss=0 --delay=0 --Gname=AOV
```
Output: 

 ```
    Overal Quality: 4.141333836065071
    Interaction Quality: 4.620955482415757
    Temporal Video Quality: 4.64
    Overall Quality for low sensitive game ( CyberSphere ): 4.196
    Overall Quality for high sensitive game ( AOV ): 4.141333836065071

 ```

# Prepration 
Install python and pip, if they are not already installed. Follow the platform specific installation instructions. The following step should be performed to prepare the setup.
```
    git clone https://github.com/stootaghaj/OMMOG.git 
    pip install -r requirements.txt
```


## Citation 
It will be added upon acceptance of paper.


## Contributors 

The work has been done by TU Brelin, Quality and Usability Lab. However, other researchers contributed to this work apart from TU Berlin.

The current code is made available to test the model. It has to be noted that code is different than the original code and might include bugs. Please contact saman.zadtootaghaj@qu.tu-berlin.de if any issues is found. 

- [Steven Schmidt](https://www.qu.tu-berlin.de/menue/team/researchers/steven_schmidt/)
- [Saman Zadtootaghaj](https://www.qu.tu-berlin.de/menue/team/researchers/zadtootahaj_saman/)


## License 


MIT License

Copyright 2021 (c) QU Lab.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

