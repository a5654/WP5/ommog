#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  7 13:54:29 2020

@author: saman
"""

from __future__ import division

import numpy as np
import pandas as pd
import argparse

            
def MOSfromR_Value(Q):
    MOS_MAX = 4.64;
    MOS_MIN = 1.3;
    MOS = MOS_MIN + (MOS_MAX-MOS_MIN)/100*Q + Q*(Q-60)*(100-Q)* 7.0e-6
    return MOS.clip(min=1.3, max=4.64)
       
#transform data from R to MOS  for an array of values

def MOSfromR(Q):
    MOS = np.zeros(Q.shape)
    MOS_MAX = 4.64;
    MOS_MIN = 1.3;
    for i in range(len(Q)):
        if (Q[i] > 0 and Q[i] < 100):
            MOS[i] = MOS_MIN + (MOS_MAX-MOS_MIN)/100*Q[i] + Q[i]*(Q[i]-60)*(100-Q[i])* 7.0e-6
        elif (Q[i] >= 100):
            MOS[i] = MOS_MAX
        else:
            MOS[i] = MOS_MIN
        return MOS.clip(min=1.3, max=4.64)
    
 
def TVQ(delay, plr, coeff):

    IVD= coeff[0]+coeff[1]/(1+np.exp(coeff[2]*delay)) + np.exp(coeff[3]*plr)
    return IVD.clip(min=0, max=70)

def INPQ(delay, plr, coeff):
        
    IQR= coeff[0]+coeff[1]/(1+np.exp(coeff[2]- coeff[3]*delay)) + coeff[3]*plr**2
    
    return IQR.clip(min=0, max=70)


def GameModel(TVQp, INPQp):
     
    R = 80 - 0.459*TVQp - 0.602*INPQp;
    
    return R
    
def OverallModel(TVQp, INPQp):
        
    R = 80 - 0.459*TVQp - 0.602*INPQp
    
    return R

def minmax(GameDicRev, delay, plr, Icoef, TVQcoef):
    QoE = [0, 1, 2, 3, 4, 5];
    for i in range(6):
        
        QoE[i]= GameModel(TVQ(delay, plr, TVQcoef[i]), INPQ(delay, plr, Icoef[i]));
        
    min_value = min(QoE[:]);
    min_index = QoE.index(min_value);
    
    max_value = max(QoE[:]);
    max_index = QoE.index(max_value);
    
    return min_value, max_value, GameDicRev[min_index], GameDicRev[max_index];

def test_model(delay, plr, Gname):
    # set the coefficients 
    GameDic = {'AOV':0, 'CyberSphere':1, 'Fortnite':2, 'PES':3, 'PUBG':4, 'Vainglory':5};
    GameDicRev = { 0: 'AOV', 1:'CyberSphere', 2:'Fortnite', 3:'PES', 4:'PUBG', 5:'Vainglory'};
        #GameRevDic = {'ID': (0, 1, 2, 3, 4, 5), 'Name': ('AOV', 'CyberSphere', 'Fortnite', 'PES', 'PUBG', 'Vainglory')};
        
    GameDataframe = [['AOV', 0], ['CyberSphere', 1], ['Fortnite', 2], ['PES', 3], ['PUBG', 4], ['Vainglory', 5]] ;
    GameDataframe = pd.DataFrame(GameDataframe, columns = ['Name', 'ID']);
    
    # input quality coef    
    Icoef = [[-2.563, 45.9, 2.098, 0.006033, 0.006846], 
            [-18.81, 56.42, 0.9425, 0.003735, 0.01031],
            [-18.61, 62.92, 1.161, 0.004283, 0.006957],
            [-18.81, 35.23, -0.001081, 0.004541, 0.003591],
            [-3.28, 44.85, 2.294, 0.003735, 0.003806],  
            [-18.81, 56.42, 0.9425, 0.006879, 0.01163],
            [-7.39, 49.39, 1.838, 0.006026, 0.007837]];
    #temporal video quality coef
    TVQcoef = [[-51, 100	, -0.002669, 0.05869],  
        [-51, 100, -0.00295, 0.07178],
        [-51, 100, -0.001894, 0.06206],
        [-51, 100, -0.0008833, 0.06409],
        [-51, 100, -0.001007, 0.05229],
        [-51, 100, -0.005148, 0.07178],
        [-51, 100, -0.002596, 0.06436]];
    
    if Gname == 'none':  
        TVQ_value = TVQ(delay, plr, TVQcoef[6]);
        INPQ_value = INPQ(delay, plr, Icoef[6]);
        R_value = OverallModel(TVQ_value, INPQ_value);
    else:    
        TVQ_value = TVQ(delay, plr, TVQcoef[GameDic[Gname]]);
        INPQ_value = INPQ(delay, plr, Icoef[GameDic[Gname]]);
        R_value = GameModel(TVQ_value, INPQ_value);
    
    [min_value, max_value, Gamemin, Gamemax] = minmax(GameDicRev, delay, plr, Icoef, TVQcoef);
        
    print("Overal Quality:", MOSfromR_Value(R_value));
    print("Interaction Quality:",MOSfromR_Value(100-INPQ_value)) 
    print("Temporal Video Quality:",MOSfromR_Value(100-TVQ_value)) 
    print("Overall Quality for low sensitive game (",Gamemax,"):", MOSfromR_Value(max_value));
    print("Overall Quality for high sensitive game (",Gamemin,"):", MOSfromR_Value(min_value)) 

    return MOSfromR_Value(R_value)

if __name__== "__main__":

    parser = argparse.ArgumentParser()
    
    parser.add_argument('-dl', '--delay', action='store', dest='delay', default=100 ,
                    help='Specify the bitrate of video', type=int)
                    
    parser.add_argument('-pl', '--packetloss', action='store', dest='packetloss', default=0 ,
                    help='Specify the coding resulotion of video', type=float)
    
    parser.add_argument('-GN', '--Gname', action='store', dest='Gname', default='none' ,
                    help='Specify the interaction complexity class of model, Low, Medium, High')
    
    values = parser.parse_args()

    test_model(values.delay, values.packetloss, values.Gname);

